import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from './pipes/pipes.module';
import { QRCodeModule } from 'angular2-qrcode';
import { BnNgIdleService } from 'bn-ng-idle';
import { AccessProviders } from 'src/app/providers/access-providers';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { ComponentsModule } from './components/components.module';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, 
            IonicModule.forRoot(), 
            IonicStorageModule.forRoot(),
            AppRoutingModule,
            HttpClientModule,
            PipesModule,
            ComponentsModule,
            QRCodeModule],
  providers: [
    StatusBar,
    SplashScreen,
    BnNgIdleService,
    QRScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Clipboard, AccessProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
