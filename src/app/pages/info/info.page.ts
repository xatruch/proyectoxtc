import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  @Input() id;
  @Input() valor;
  @Input() slug;
  @Input() billetera;
  @Input() create_at;
  @Input() tipo;
 
 
  
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async cerrarModal(){
    await this.modalCtrl.dismiss();
  }

}
