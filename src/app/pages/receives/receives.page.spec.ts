import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReceivesPage } from './receives.page';

describe('ReceivesPage', () => {
  let component: ReceivesPage;
  let fixture: ComponentFixture<ReceivesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReceivesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
