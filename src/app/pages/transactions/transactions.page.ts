import { Component, OnInit } from '@angular/core';
import { ToastController, NavController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';


@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {

  datastorageb: any;
  direccion: string;
 

  constructor(private storage:Storage,
              private toastCtl: ToastController,
              private navCtrl: NavController,
              private router: Router,
              private menuCtrl: MenuController,
              private clipboard: Clipboard) { }

  ngOnInit(){
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }
  
    ionViewDidEnter(){
    this.storage.get('storage_billetera').then((res)=>{
          //console.log(res);
          this.datastorageb = res;
          this.direccion = this.datastorageb.direccion;
    });
  }
  
  sends(){
    this.router.navigate(['/sends']);
   }

   copiar() {
    this.clipboard.copy(this.direccion);
    this.presentToast("Copiado");
  }

  
  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 500,
      position: 'middle',
      cssClass: 'toast'
    });
    toast.present();
  }

   //logout
   async logout(){
    this.storage.clear();
    this.navCtrl.navigateRoot(['/index']);
    const toast = await this.toastCtl.create({
      message: '!Salió con éxito!, nos vemos pronto, Feliz dia.. ',
      duration: 2500,
      position: 'top',
      cssClass: 'toastss'
    });
    toast.present();
  }

}
