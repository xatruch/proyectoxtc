import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, MenuController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  datastorage: any;
  id: string;
  name: string;

  coins: any = [];
  limitc: number = 10;
  startc: number = 0;

  wallets: any = [];
  xtcs: any = [];
  tracks: any = [];
  apikey: string = '?api_key=key_xtruch_prod_gfL52nZPvJQQgHT0Mjk07ivAKw3a0KYe';  

  constructor(private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private menuCtrl: MenuController,
              private accessPrvds: AccessProviders,
              private storage: Storage,
              private navCtrl: NavController) { }

  ngOnInit() {
  }

  openMenu(){
    this.menuCtrl.toggle();
  }

  ionViewDidEnter(){
    this.storage.get('storage_local').then((res)=>{
          //console.log(res);
          this.datastorage = res;
          this.id = this.datastorage.id;
          this.name = this.datastorage.name;
    });

    this.startc = 0;
    this.coins = [];
    this.wallets = [];
    this.xtcs = [];
    this.tracks = [];
    this.loadcoins();
  }

  async doRefresh(event){
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
  });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }

  loadData(){
    this.startc += this.limitc;
    setTimeout(()=>{
      this.loadcoins().then(()=>{
         /// event.target.complete();
      });

    }, 500);
  }


  //coins

  async loadcoins(){

    return new Promise(resolve => {
       let body ={
        startc: this.startc,
        limitc: this.limitc
       }
      this.accessPrvds.postData(body, `/monedas`).subscribe((res:any)=>{
             //console.log(res.message);
            for(let datac of res.message ){
              this.coins.push(datac);
            }
            resolve(true);

            this.accessPrvds.postData(body,`/xtc`).subscribe((res:any)=>{
              //console.log(res.xtc);
              for(let datas of res.xtc ){
                this.xtcs.push(datas);
              }
          resolve(true);
      });
              this.accessPrvds.postData(body,`/wallet/${this.id}`).subscribe((res:any)=>{
                //console.log(res.wallet);
                  this.wallets.push(res.wallet);
                  this.storage.set('storage_billetera', res.wallet);
              resolve(true);
        });
                this.accessPrvds.postData(body,`/transaction/${this.id}`).subscribe((res:any)=>{
                  //console.log(res.track);
                  for(let datat of res.track ){
                    this.tracks.push(datat);
                    this.storage.set('storage_transac', res.track);
                  }
                resolve(true);
          });
        
      });

    });

  }


  //logout
  async logout(){
    this.storage.clear();
    this.navCtrl.navigateRoot(['/index']);
    const toast = await this.toastCtl.create({
      message: '!Salió con éxito!, nos vemos pronto, Feliz dia.. ',
      duration: 2500,
      position: 'top',
      cssClass: 'toastss'
      
    });
    toast.present();
  }


}
