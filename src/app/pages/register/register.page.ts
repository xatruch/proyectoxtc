import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  name: string = "";
  username: string = "";
  email: string = "";
  telephone: string = "";
  date: string = "";
  password: string = "";
  re_pass: string = "";
  apikey: string = '?api_key=key_xtruch_prod_gfL52nZPvJQQgHT0Mjk07ivAKw3a0KYe';


  disabledButton;

  showPassword = false;
  passwordToggleIcon = 'eye';

  constructor(private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private accessPrvds: AccessProviders) { }

  ngOnInit() {
  }

  togglePassword(): void{
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye'){
      this.passwordToggleIcon = 'eye-off';
    }else{
      this.passwordToggleIcon = 'eye';
    }
  }

  ionViewDidEnter(){
    this.disabledButton = false;
  }

  async tryRegister(){
    if(this.name ==""){
      this.presentToast('Su Nombre es requerido');
    }else if(this.username ==""){
      this.presentToast('Su Usuario es requerido');
    }else if(this.email ==""){
      this.presentToast('Su Email es requerido');
    }else if(this.telephone ==""){
      this.presentToast('Su Numero de telefono es requerido');
    //}else if(this.date ==""){
      //this.presentToast('Su Fecha de nacimiento es requerida');
    }else if(this.password ==""){
      this.presentToast('Su contraseña es requerida');
    }else if(this.re_pass !=this.password){
      this.presentToast('Su confirmación de contraseña es incorrecta');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtl.create({
        message: 'Cargando.....',
      });
      loader.present();

      return new Promise(resolve => {
         let body = {
           name: this.name,
           username: this.username,
           email: this.email,
           telephone: this.telephone,
          // date: this.date,
           password: this.password
         }

         this.accessPrvds.postData(body,`/register`).subscribe((res:any)=>{
          //console.log(body);
          if(res.ok == true){
            //console.log(res);
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast('El Usuario se ha registrado correctamente');
            this.router.navigate(['/login']);
          }else {
            loader.dismiss();
            //console.log(res);
            this.disabledButton = false;
            this.presentToast('Usuario o Email ya exiten, favor no dejar campos vacios');
          }

         },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('Error respuesta servidor');
         });

      });
    }

  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 1500,
      position: 'top',
      cssClass: 'toastss'
    });
    toast.present();
  }

  async presentAlert(a) {
    const alert = await this.alertCtl.create({
      header: a,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cerrar',
          handler: (blah) => {
          }
        }, {
          text: 'Intentar nuevamente',
          handler: () => {
            this.tryRegister();
          }
        }
      ]
    });

    await alert.present();
  }


}
