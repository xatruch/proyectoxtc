import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-select',
  templateUrl: './select.page.html',
  styleUrls: ['./select.page.scss'],
})
export class SelectPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Login(){
    this.router.navigate(['/login']);
  }

  Register(){
    this.router.navigate(['/register']);
  }

}
