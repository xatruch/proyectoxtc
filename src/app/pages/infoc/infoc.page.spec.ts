import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfocPage } from './infoc.page';

describe('InfocPage', () => {
  let component: InfocPage;
  let fixture: ComponentFixture<InfocPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfocPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfocPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
