import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from 'src/app/providers/access-providers';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-infoc',
  templateUrl: './infoc.page.html',
  styleUrls: ['./infoc.page.scss'],
})
export class InfocPage implements OnInit {

  @Input() id;
  @Input() name;
  @Input() email;
  @Input() telephone;
  @Input() direccion;
 
  datastorage: any;
  idc: string;

  disabledButton;

  constructor(private modalCtrl: ModalController,
    private toastCtl: ToastController,
    private loadingCtl: LoadingController,
    private alertCtl: AlertController,
    private accessPrvds: AccessProviders,
    private storage: Storage) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.disabledButton = false;
    this.storage.get('storage_local').then((res)=>{
      //console.log(res);
     this.datastorage = res;
     this.idc = this.datastorage.id;
  });

}

async tryEditar(){

  if(this.name ==""){
    this.presentToast('Nombre es requerido');
  }else if(this.direccion ==""){
    this.presentToast('Código de billetara es requerido');
  }else{
    this.disabledButton = true;
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
    });
    loader.present();
    
    return new Promise(resolve => {
      let body = {
        namec: this.name,
        emailc: this.email,
        telephonec: this.telephone,
        direccionc: this.direccion,
        idc: this.id,
      }

        this.accessPrvds.postData(body,`/editarC/${this.id}`).subscribe((res:any)=>{
          //console.log(body);
          if(res.ok == true){
           //console.log(res);
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(res.message);
            this.cerrarModal();
          }else {
            loader.dismiss();
            //console.log(res);
            this.disabledButton = false;
            this.presentToast(res.message);
          }

        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('Error respuesta servidor');
        });
    });
  }
}

async doRefreshdel(event){
  const loader = await this.loadingCtl.create({
    message: 'Cargando.....',
});
  loader.present();
  this.ionViewDidEnter();
  loader.dismiss();
}

async presentToast(a){
  const toast = await this.toastCtl.create({
    message: a,
    duration: 1500,
    position: 'top',
    cssClass: 'toastss'
  });
  toast.present();
}

async presentAlert(a) {
  const alert = await this.alertCtl.create({
    header: a,
    backdropDismiss: false,
    buttons: [
      {
        text: 'Cerrar',
        handler: (blah) => {
        }
      }, {
        text: 'Intentar nuevamente',
        handler: () => {
          this.tryEditar();
        }
      }
    ]
  });

  await alert.present();
}

  async cerrarModal(){
    await this.modalCtrl.dismiss();
  }


}
