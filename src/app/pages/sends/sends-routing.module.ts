import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendsPage } from './sends.page';

const routes: Routes = [
  {
    path: '',
    component: SendsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendsPageRoutingModule {}
