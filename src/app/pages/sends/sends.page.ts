import { Component, OnInit } from '@angular/core';
import { ToastController, AlertController, Platform, ModalController } from '@ionic/angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { SendscPage } from '../sendsc/sendsc.page';

@Component({
  selector: 'app-sends',
  templateUrl: './sends.page.html',
  styleUrls: ['./sends.page.scss'],
})
export class SendsPage implements OnInit {

  cantidad: string = "";
  denvio: string = "";
  qrScan: any;

  constructor(private toastCtl: ToastController,
    private alertCtl: AlertController,
    private qrScanner: QRScanner,
    private modalCtrl: ModalController,
    public platform: Platform) { 
     
    }

ngOnInit() {
}

scan(){

  this.qrScanner.prepare()
  .then((status: QRScannerStatus) => {
     if (status.authorized) {
       // camera permission was granted


       // start scanning
       let scanSub = this.qrScanner.scan().subscribe((text: string) => {
         console.log('Scanned something', text);
         this.denvio = text;

         this.qrScanner.hide(); // hide camera preview
         scanSub.unsubscribe(); // stop scanning
       });

     } else if (status.denied) {
       // camera permission was permanently denied
       // you must use QRScanner.openSettings() method to guide the user to the settings page
       // then they can grant the permission from there
     } else {
       // permission was denied, but not permanently. You can ask for permission again at a later time.
     }
  })
  .catch((e: any) => console.log('Error is', e));
  //this.qrScanner.prepare()
//.then((status: QRScannerStatus) => {
  // if (status.authorized) {
     // camera permission was granted
     // start scanning
    // this.qrScanner.show();
    // this.qrScan = document.getElementsByTagName('body')[0].style.opacity = '0';
    // this.qrScan = this.qrScanner.scan().subscribe((textFound: string)=>{
    //  document.getElementsByTagName('body')[0].style.opacity = '1';
     //  this.denvio = textFound;
    //   this.qrScan.destroy();
     //  this.qrScan.hide();
     //  this.qrScan.cancel();
     //  this.qrScan.unsubscribe();
     //  this.qrScanner.destroy(); 
    // });

   //} else if (status.denied) {
     // camera permission was permanently denied
     // you must use QRScanner.openSettings() method to guide the user to the settings page
     // then they can grant the permission from there
  // } else {
     // permission was denied, but not permanently. You can ask for permission again at a later time.
   //}
//})
//.catch((e: any) => console.log('Error lectura QR', e));

}

async trySend(){
  if(this.cantidad == ''){
     this.presentToast('La cantidad es requerida');
  }else if(this.denvio == ''){
    this.presentToast('La dirección es requerida');
  }else { 
    const modal = await this.modalCtrl.create({
      component: SendscPage,
      componentProps:{
        cantidad: this.cantidad,
        denvio: this.denvio
      }
    });
     await modal.present();
  }

}

async presentToast(a){
  const toast = await this.toastCtl.create({
    message: a,
    duration: 2500,
    position: 'top',
    cssClass: 'toasts'
  });
  toast.present();
}

async presentQR(a) {
  const alert = await this.alertCtl.create({
    header: a,
    backdropDismiss: false,
    buttons: [
      {
        text: 'Cerrar',
        handler: (blah) => {
        }
      }, {
        text: 'Intentar nuevamente',
        handler: () => {
          this.scan();
        }
      }
    ]
  });

  await alert.present();
}

}
