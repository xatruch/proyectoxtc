import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SendsPage } from './sends.page';

describe('SendsPage', () => {
  let component: SendsPage;
  let fixture: ComponentFixture<SendsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SendsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
