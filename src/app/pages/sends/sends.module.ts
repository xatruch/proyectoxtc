import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendsPageRoutingModule } from './sends-routing.module';
import { SendsPage } from './sends.page';
import { SendscPageModule } from '../sendsc/sendsc.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendsPageRoutingModule,
    SendscPageModule
  ],
  declarations: [SendsPage]
})
export class SendsPageModule {}
