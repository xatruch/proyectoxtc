import { Component, OnInit } from '@angular/core';
import { ToastController, NavController, LoadingController, AlertController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AccessProviders } from 'src/app/providers/access-providers';
import { ModalController } from '@ionic/angular'
import { InfoPage } from '../info/info.page';


@Component({
  selector: 'app-movements',
  templateUrl: './movements.page.html',
  styleUrls: ['./movements.page.scss'],
})
export class MovementsPage implements OnInit {

  datastorageb: any;
  id: string;

  limitc: number = 100;
  startc: number = 0;
  tracks: any = [];
  tipos: any =[];
  apikey: string = '?api_key=key_xtruch_prod_gfL52nZPvJQQgHT0Mjk07ivAKw3a0KYe';

  constructor(private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private accessPrvds: AccessProviders,
              private storage: Storage,
              private navCtrl: NavController,
              private menuCtrl: MenuController,
              private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }

  ionViewDidEnter(){
    this.storage.get('storage_billetera').then((res)=>{
          //console.log(res);
          this.datastorageb = res;
          this.id = this.datastorageb.id;
    });
  
    this.startc = 0;
    this.tracks = [];
    this.tipos =[];
  
    this.loadtracks();
  }
  
  async doRefresh(event){
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
  });
    loader.present();
  
    this.ionViewDidEnter();
    event.target.complete();
  
    loader.dismiss();
  }
  
  loadData(){
    this.startc += this.limitc;
    setTimeout(()=>{
      this.loadtracks().then(()=>{
         /// event.target.complete();
      });
  
    }, 500);
  }
  
  
   //coins
  
   async loadtracks(){
  
    return new Promise(resolve => {
       let body ={
        startc: this.startc,
        limitc: this.limitc
       }
          this.accessPrvds.postData(body,`/transaction/${this.id}`).subscribe((res:any)=>{
            //console.log(res.track);
            for(let datat of res.track ){
                 this.tracks.push(datat);
                 this.tipos.push(datat.tipo);
               }
              resolve(true);
          });
        
      });
  
  }

  async abreModal(id: string, valor: string, slug: string, billetera: string, create_at: string, tipo: string){
    const modal = await this.modalCtrl.create({
      component: InfoPage,
      componentProps: {
        id,
        valor,
        slug,
        billetera,
        create_at,
        tipo
      }
    });
    return await modal.present();
  }
  
  //logout
  async logout(){
    this.storage.clear();
    this.navCtrl.navigateRoot(['/index']);
    const toast = await this.toastCtl.create({
        message: '!Salió con éxito!, nos vemos pronto, Feliz dia.. ',
        duration: 2500,
        position: 'top',
        cssClass: 'toastss'
      });
      toast.present();
  }

}
