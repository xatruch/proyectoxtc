import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes =[
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'settings',
        loadChildren: '../settings/settings.module#SettingsPageModule'
      },
      {
        path: 'terms',
        loadChildren: '../terms/terms.module#TermsPageModule'
      },
      {
        path: 'acerca',
        loadChildren: '../acerca/acerca.module#AcercaPageModule'
      },
    ]
  },
  {
     path: '',
     redirectTo: '/menu/settings'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
