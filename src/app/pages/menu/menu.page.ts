import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      "title": "Configuración",
      "url": "/menu/settings",
      "icon": "cog-sharp"
    },
    {
      "title": "Términos y condiciones",
      "url": "/menu/terms",
      "icon": "reader-sharp"
    },
    {
      "title": "Acerca",
      "url": "/menu/acerca",
      "icon": "information-circle-outline"
    }         
  ];

  selectedPath = '';

  constructor(private router: Router) { 
    this.router.events.subscribe(( event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
  }

}
