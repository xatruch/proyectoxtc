import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AccessProviders } from 'src/app/providers/access-providers';

@Component({
  selector: 'app-infot',
  templateUrl: './infot.page.html',
  styleUrls: ['./infot.page.scss'],
})
export class InfotPage implements OnInit {

  datastorageb: any;
  cantidad: string = "";
  denvio: string = "";
  password: string = "";
  direccion: string;
  id: string;
  saldo: number;

  constructor(private modalCtrl: ModalController,
              private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private storage:Storage,
              private accessPrvds: AccessProviders) { }

  ngOnInit() {
  }

  async cerrarModal(){
    await this.modalCtrl.dismiss();
  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 2500,
      position: 'top',
      cssClass: 'toasts'
    });
    toast.present();
  }

  async presentAlert(a) {
    const alert = await this.alertCtl.create({
      header: a,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cerrar',
          handler: (blah) => {
          }
        }, {
          text: 'Intentar nuevamente',
          handler: () => {
            //this.trySends();
          }
        }
      ]
    });

    await alert.present();
  }


  ionViewDidEnter(){
    this.storage.get('storage_billetera').then((res)=>{
          //console.log(res);
          this.datastorageb = res;
          this.id = this.datastorageb.id;
          this.direccion = this.datastorageb.direccion;
          this.saldo = this.datastorageb.saldo;
    });
  }

}
