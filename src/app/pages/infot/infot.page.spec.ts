import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfotPage } from './infot.page';

describe('InfotPage', () => {
  let component: InfotPage;
  let fixture: ComponentFixture<InfotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
