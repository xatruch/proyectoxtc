import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from 'src/app/providers/access-providers';


@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  version: string = "1.0.0.2"
  apikey: string = '?api_key=key_xtruch_prod_gfL52nZPvJQQgHT0Mjk07ivAKw3a0KYe'; 

  constructor(private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private accessPrvds: AccessProviders) { }

  ngOnInit() {
  }

  async select(){

    const loader = await this.loadingCtl.create({
      message: 'Comprobar si hay actualizaciones disponibles',
    });
    loader.present();
    
    return new Promise(resolve => {
      let body = {
        version: this.version
      }
      //console.log(body);
      //console.log(`${this.apikey}${this.apikey}`);
      this.accessPrvds.postData(body, `/versione`).subscribe((res:any)=>{
        //console.log(res);
       if(res.ok == true){
         //console.log(res);
         loader.dismiss();
         this.presentToast(res.message);
         this.router.navigate(['/select']);
       }else {
         loader.dismiss();
         //console.log(res);
         this.presentToast(res.message);
       }

      },(err)=>{
       loader.dismiss();
       this.presentAlert('Error respuesta servidor');
      });

   });

  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 3000,
      position: 'top',
      cssClass: 'toastss'
    });
    toast.present();
  }

  async presentAlert(a) {
    const alert = await this.alertCtl.create({
      header: a,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cerrar',
          handler: (blah) => {
          }
        }, {
          text: 'Intentar nuevamente',
          handler: () => {
            this.select();
          }
        }
      ]
    });

    await alert.present();
  }

  terms(){
    this.router.navigate(['/terms']);
  }

}
