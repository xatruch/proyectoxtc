import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendscPage } from './sendsc.page';

const routes: Routes = [
  {
    path: '',
    component: SendscPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendscPageRoutingModule {}
