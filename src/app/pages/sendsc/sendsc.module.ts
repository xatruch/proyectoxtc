import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendscPageRoutingModule } from './sendsc-routing.module';

import { SendscPage } from './sendsc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendscPageRoutingModule
  ],
  declarations: [SendscPage]
})
export class SendscPageModule {}
