import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SendscPage } from './sendsc.page';

describe('SendscPage', () => {
  let component: SendscPage;
  let fixture: ComponentFixture<SendscPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendscPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SendscPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
