import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from 'src/app/providers/access-providers';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sendsc',
  templateUrl: './sendsc.page.html',
  styleUrls: ['./sendsc.page.scss'],
})
export class SendscPage implements OnInit {
  
  @Input() cantidad;
  @Input() denvio;
  password: string = "";
  direccion: string;
  id: string;
  saldo: number;
  datastorage: any;
  disabledButton;
  showPassword = false;
  passwordToggleIcon = 'eye';
  
  constructor(private modalCtrl: ModalController,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private accessPrvds: AccessProviders,
              private storage: Storage,
              private router: Router) { }

  ngOnInit() {
  }

  togglePassword(): void{
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye'){
      this.passwordToggleIcon = 'eye-off';
    }else{
      this.passwordToggleIcon = 'eye';
    }
  }

  async trySends(){
    if(this.cantidad ==""){
       this.presentToast('La cantidad es requerida');
    }else if(this.denvio ==""){
      this.presentToast('La dirección es requerida');
    }else if(this.password ==""){
      this.presentToast('Su contraseña es requerida');
    }else { 
      const loader = await this.loadingCtl.create({
        message: 'Procesando envio...',
      });
      loader.present();

      return new Promise(resolve => {
         let body = {
           cantidad: this.cantidad,
           denvio: this.denvio,
           password: this.password,
           id: this.id,
           direccion: this.direccion,
           saldo: this.saldo
         }
         console.log(body);
         this.accessPrvds.postData(body, `/transaction`).subscribe((res:any)=>{

          if(res.ok == true){
            console.log(res);
            loader.dismiss();
            this.presentToast(res.message);
            this.router.navigate(['/tabs/movements']);
            this.modalCtrl.dismiss({
              chks:'ok'
            });
            
          }else {
            loader.dismiss();
            console.log(res);
            this.presentToast(res.message);
          }

         },(err)=>{
          loader.dismiss();
          this.presentAlert('Error respuesta servidor');
         });

      });
    }

  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 2500,
      position: 'top',
      cssClass: 'toasts'
    });
    toast.present();
  }

  async presentAlert(a) {
    const alert = await this.alertCtl.create({
      header: a,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cerrar',
          handler: (blah) => {
          }
        }, {
          text: 'Intentar nuevamente',
          handler: () => {
            this.trySends();
          }
        }
      ]
    });

    await alert.present();
  }

  ionViewDidEnter(){
      this.storage.get('storage_billetera').then((res)=>{
        console.log(res);
      this.datastorage = res;
      this.id = this.datastorage.id;
      this.direccion = this.datastorage.direccion;
      this.saldo = this.datastorage.saldo;
    });
  }

  async cerrarModal(){
    await this.modalCtrl.dismiss();
    
  }

}
