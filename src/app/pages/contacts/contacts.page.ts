import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, ModalController, MenuController } from '@ionic/angular';
import { AccessProviders } from 'src/app/providers/access-providers';
import { AddPage } from '../add/add.page';
import { Storage } from '@ionic/storage';
import { InfocPage } from '../infoc/infoc.page';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {

  datastorage: any;
  id: string;
  textoBuscar ='';

  contacts: any = [];
  limitc: number = 10;
  startc: number = 0;

  constructor(private router: Router,
    private toastCtl: ToastController,
    private loadingCtl: LoadingController,
    private alertCtl: AlertController,
    private accessPrvds: AccessProviders,
    private storage: Storage,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }

  buscar (event){
    this.textoBuscar = event.detail.value;
  }

  ionViewDidEnter(){
    this.storage.get('storage_local').then((res)=>{
          //console.log(res);
          this.datastorage = res;
          this.id = this.datastorage.id;
    });

    this.startc = 0;
    this.contacts = [];
    this.loadcontacts();
  }

  async doRefresh(event){
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
  });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }
  

  loadData(){
    this.startc += this.limitc;
    setTimeout(()=>{
      this.loadcontacts().then(()=>{
         /// event.target.complete();
      });

    }, 500);
  }

  async doRefreshdel(event){
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
  });
    loader.present();
    this.ionViewDidEnter();
    loader.dismiss();
  }



  async loadcontacts(){

    return new Promise(resolve => {
       let body ={
        startc: this.startc,
        limitc: this.limitc
       }
      this.accessPrvds.postData(body, `/contacts/${this.id}`).subscribe((res:any)=>{
             //console.log(res.message);
            for(let datac of res.contact ){
              this.contacts.push(datac);
            }
            resolve(true);
      });

    });

  }

  async add(){
    const modal = await this.modalCtrl.create({
      component: AddPage,
      componentProps:{
      }
    });
     await modal.present();
     await modal.onDidDismiss();
     await this.doRefreshdel(event);
  }

  async update(id: string, name: string, email: string, telephone: string, direccion: string){
    const modal = await this.modalCtrl.create({
      component: InfocPage ,
      componentProps:{
        id,
        name,
        email,
        telephone,
        direccion
      }
    });
     await modal.present();
     await modal.onDidDismiss();
     await this.doRefreshdel(event);
  }

  async delete(id: string){
    const loader = await this.loadingCtl.create({
      message: 'Cargando.....',
    });
    loader.present();

    return new Promise(resolve => {
      let body ={
       id: id
      }
     this.accessPrvds.postData(body, `/destroyC/${id}`).subscribe((res:any)=>{
          if(res.ok==true){
            this.presentToast(res.contact);
            this.doRefreshdel(event);
            loader.dismiss();
          }else{
            this.presentToast(res.contact);
          }
     });

   });

  }

  //logout
  async logout(){
    this.storage.clear();
    this.navCtrl.navigateRoot(['/index']);
    const toast = await this.toastCtl.create({
      message: '!Salió con éxito!, nos vemos pronto, Feliz dia.. ',
      duration: 2500,
      position: 'top',
      cssClass: 'toastss'
      
    });
    toast.present();
  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 1500,
      position: 'top',
      cssClass: 'toastss'
    });
    toast.present();
  }

  async presentAlert(id: string){
    const alert = await this.alertCtl.create({
      header: 'Confirmación?',
      message: 'Desea eliminar este contacto?',
      buttons: [{
          text: 'Cancelar',
          handler: (blah) => {
          }
        }, 
        {
          text: 'Aceptar',
          handler: () => {
            this.delete(id);
          }

        }],
    });
    await alert.present();
}

}
