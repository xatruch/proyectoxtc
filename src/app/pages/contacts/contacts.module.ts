import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactsPageRoutingModule } from './contacts-routing.module';

import { ContactsPage } from './contacts.page';
import { AddPageModule } from '../add/add.module';
import { PipesModule } from '../../pipes/pipes.module';
import { InfocPageModule } from '../infoc/infoc.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactsPageRoutingModule,
    AddPageModule,
    InfocPageModule,
    PipesModule
  ],
  declarations: [ContactsPage]
})
export class ContactsPageModule {}
