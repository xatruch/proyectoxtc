import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AccessProviders } from 'src/app/providers/access-providers';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  namec: string = "";
  emailc: string = "";
  telephonec: string = "";
  direccionc: string = "";
  datastorage: any;
  id: string;

  disabledButton;
  

  constructor(private modalCtrl: ModalController,
              private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private accessPrvds: AccessProviders,
              private storage: Storage) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
      this.disabledButton = false;
      this.storage.get('storage_local').then((res)=>{
        //console.log(res);
       this.datastorage = res;
       this.id = this.datastorage.id;
    });

  }

  async tryRegisterC(){

    if(this.namec ==""){
      this.presentToast('Nombre es requerido');
    }else if(this.direccionc ==""){
      this.presentToast('Código de billetara es requerido');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtl.create({
        message: 'Cargando.....',
      });
      loader.present();
      
      return new Promise(resolve => {
        let body = {
          namec: this.namec,
          emailc: this.emailc,
          telephonec: this.telephonec,
          direccionc: this.direccionc,
          id: this.id
        }

          this.accessPrvds.postData(body,`/registerC`).subscribe((res:any)=>{
            //console.log(body);
            if(res.ok == true){
              //console.log(res);
              loader.dismiss();
              this.disabledButton = false;
              this.presentToast(res.message);
              this.cerrarModal();
            }else {
              loader.dismiss();
              //console.log(res);
              this.disabledButton = false;
              this.presentToast(res.message);
            }

          },(err)=>{
            loader.dismiss();
            this.disabledButton = false;
            this.presentAlert('Error respuesta servidor');
          });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 1500,
      position: 'top',
      cssClass: 'toastss'
    });
    toast.present();
  }

  async presentAlert(a) {
    const alert = await this.alertCtl.create({
      header: a,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cerrar',
          handler: (blah) => {
          }
        }, {
          text: 'Intentar nuevamente',
          handler: () => {
            this.tryRegisterC();
          }
        }
      ]
    });

    await alert.present();
  }

  async cerrarModal(){
    await this.modalCtrl.dismiss();
  }

}
