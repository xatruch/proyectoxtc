import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = "";
  password: string = "";
  apikey: string = '?api_key=key_xtruch_prod_gfL52nZPvJQQgHT0Mjk07ivAKw3a0KYe';

  disabledButton;

  showPassword = false;
  passwordToggleIcon = 'eye';

  constructor(private router: Router,
              private toastCtl: ToastController,
              private loadingCtl: LoadingController,
              private alertCtl: AlertController,
              private accessPrvds: AccessProviders,
              private storage: Storage,
              private navCtrl: NavController) { }

  ngOnInit() {
  }

  togglePassword(): void{
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye'){
      this.passwordToggleIcon = 'eye-off';
    }else{
      this.passwordToggleIcon = 'eye';
    }
  }

  ionViewDidEnter(){
   this.disabledButton = false;
  }

  async tryLogin(){
    if(this.username ==""){
      this.presentToast('Su Usuario es requerido');
    }else if(this.password ==""){
      this.presentToast('Su contraseña es requerida');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtl.create({
        message: 'Cargando.....',
      });
      loader.present();

      return new Promise(resolve => {
         let body = {
           username: this.username,
           password: this.password
         }

         this.accessPrvds.postData(body, `/login`).subscribe((res:any)=>{

          if(res.ok == true){
            //console.log(res);
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast('Bienvenido a tu Billetera');
            this.storage.set('storage_local', res.message);
            this.navCtrl.navigateRoot(['/tabs/home']);
          }else {
            loader.dismiss();
            //console.log(res);
            this.disabledButton = false;
            this.presentToast('Usuario o contraseña incorrectos');
          }

         },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentToast('Error respuesta servidor');
         });

      });
    }

  }

  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 1500,
      cssClass: 'toasts'
    });
    toast.present();
  }


}
