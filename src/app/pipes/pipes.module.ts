import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePipe } from './image.pipe';
import { FilterPipe } from './filter.pipe';



@NgModule({
  declarations: [ImagePipe, FilterPipe],
  exports:[ImagePipe, FilterPipe],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
