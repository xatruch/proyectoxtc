import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';


@Injectable()
export class AccessProviders{
    server: string = 'http://xatruchcoin.org/api';
    //server: string = 'http://localhost/Initwallet/public/api';

    constructor(
                public http: HttpClient
    ){ }

    postData(body, file){
        let headers = new HttpHeaders({
            'Content-type': 'application/json; charset=UTF-8'
        });
        let options = {
            headers: headers
        }

        return this.http.post(this.server + file, JSON.stringify(body), options)
        .timeout(5900).map(res => res);
    }
    
}