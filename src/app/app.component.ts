import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { BnNgIdleService } from 'bn-ng-idle';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public AppPages =[
    {
      "title": "Configuración",
      "url": "/menu/settings",
      "icon": "cog-sharp"
    },
    {
      "title": "Términos y condiciones",
      "url": "/menu/terms",
      "icon": "reader-sharp"
    },
    {
      "title": "Acerca",
      "url": "/menu/acerca",
      "icon": "information-circle-outline"
    }         
  ]

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private navCtrl: NavController, 
    private bnIdle: BnNgIdleService,
    private toastCtl: ToastController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.storage.get('storage_local').then((res)=>{
      if(res == null){
        this.navCtrl.navigateRoot('/index');
      }else{
        this.navCtrl.navigateRoot('/tabs/home');
      }
    });

    this.bnIdle.startWatching(600).subscribe((res) => {
      if(res) {
        this.storage.clear();
        this.navCtrl.navigateRoot(['/index']);
        this.presentToast("Sesión expirada");
      }
    });
  }
  async presentToast(a){
    const toast = await this.toastCtl.create({
      message: a,
      duration: 2500,
      position: 'middle',
      cssClass: 'toasts'
    });
    toast.present();
  }

}
